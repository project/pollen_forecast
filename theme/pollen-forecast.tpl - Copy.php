<?php
global $base_url, $theme, $language;
//$filepath = $base_url . '/' . file_directory_path() . '/' . $theme;

$notsupported = t('Sorry, your browser does not support this');
$locationtext = $selected_location;
if (strlen($selected_location) >= 14) {
$locationtext = substr($selected_location, 0, 14);
}
$fb_language = (($language->language == 'fr') ? 'fr_CA' : 'en_US');
$polsetval =  variable_get('setpollendisplay'); // 1(Yes) or 0 {No)
?>
<input type="hidden" id="pollencount" value="<?php print $pollen_count ?>" />

<div class="today-alrgy_bg">
  <div class="todyalrgy_dtls">
    <div class="tody_alrgy_heading">
      <h1>
        <span id="alrgy_selected_day"><?php print t('Today'); ?></span>'<?php print t('s pollen forecast'); ?><sup id="alrgy_heading_plus">†</sup>
      </h1>
      <div class="locationblg">
        <div class="chngloctin">
          <a href="javascript:void(0);" title="<?php print $selected_location; ?>"><?php print $locationtext; ?></a>
        </div>
      </div>
    </div>
    <div class="todyalrgydtls">
      <?php if($polsetval == 1)  {  ?>
      <div class="forcastblg">
        <div class="polnlevelgrp">
          <div class="pollevl-medum"></div>
          <div class="forcstdays">
          <?php
          if($pollen_count > 0){
          ?>
            <ul>
              <?php
              if ($forecastinfo != 'noweather' && count($forecastinfo) > 0) {
                $loop = 0;
                foreach ($forecastinfo as $forecast) {
                  $p_text = $pollenoutlook = strtolower($forecast->pollenoutlook);
                  $day = (($loop == 0) ? t('Today') : $forecast->day);
                  $day_fulltext = (($loop == 0) ? t('Today') : $forecast->day_fulltext);

                  //$class = 'poln' . $pollenoutlook;
                  $weathertext = $weathericons[$forecast->weatherCode];
                  if (empty($weathertext)) {
                    $weathertext = t('Weather Unavailable');
                    //$p_text ='NA';
                  }
                  $weathertext = wordwrap($weathertext, 13, "<br>\n", true);
                  $class = 'poln' . $p_text;
              ?>
                <li class="pollenday <?php print "day-" . $loop; ?>" id="day-<?php print $loop;?>">
                  <ul>
                    <li class="days" id="day"><?php print t($day); ?></li>
                    <li class="<?php print $class; ?>" id="pollenlevel_day<?php print $loop; ?>"></li>
                    <li id="pollenlevel_text<?php print $loop; ?>" class="pollenlevel_text"><?php print t($p_text); ?></li>
                    <li id="temp_high<?php print $loop; ?>" class="temp_high"><?php print $forecast->tempMaxC; ?></li>
                    <li id="temp_low<?php print $loop; ?>" class="temp_low"><?php print $forecast->tempMinC; ?></li>
                    <li id="weather_code<?php print $loop; ?>" class="weather_code"><?php print $forecast->weatherCode; ?></li>
                    <li id="weather_text<?php print $loop; ?>" class="weather_text"><?php print $weathertext; ?></li>
                    <li id="days_fulltext<?php print $loop; ?>" class="day_fulltext"><?php print t($day_fulltext); ?></li>
                  </ul>
                </li>
              <?php
                $loop++;
                }
              }
              ?>
            </ul>
          <?php } ?>
          </div>
        </div>
      </div>
      <?php
      $weathertext = $weathericons[$forecastinfo[0]->weatherCode];
      if (empty($weathertext)) {
        $weathertext = t('Weather Unavailable');
      }
      $weathertext = wordwrap($weathertext, 13, "<br>\n", true);
      $padding = '';
      if (count(explode("<br>", $weathertext)) == 1) {
        $padding = 'style="padding-top:20px;"';
      }
      else if (count(explode("<br>", $weathertext)) == 2) {
        $padding = 'style="padding-top:15px;"';
      }
      ?>
      <div class="weather">
        <div class="weatherblg">
          <h2><?php print t('Weather'); ?></h2>
            <ul>
              <li class="wethricon <?php print 'code-' . (empty($forecastinfo[0]->weatherCode) ? 'none' : $forecastinfo[0]->weatherCode); ?>"></li>
              <li <?php print $padding; ?> class="wetrdtls"><strong><?php print $weathertext; ?></strong></li>
              <li class="high">
                <span><?php print t('High'); ?></span>
                <h3 class="temphigh_text"><?php print $forecastinfo[0]->tempMaxC; ?></h3>
              </li>
              <li class="low">
                <span><?php print t('Low'); ?></span>
                <h3 class="templow_text"><?php print $forecastinfo[0]->tempMinC; ?></h3>
              </li>
            </ul>
        </div>
        <?php } else { ?>
        <div class="noforcastblg"></div>
        <div class="weather">
          <div class="weatherblg">
            <h2><?php print t('Weather'); ?></h2>
            <ul>
              <li class="nowethricon"></li>
              <li class="high">
                <span><?php print t('High'); ?></span>
                <h3 class="temphigh_text"><?php print $forecastinfo[0]->tempMaxC; ?></h3>
              </li>
              <li class="low">
                <span><?php print t('Low'); ?></span>
                <h3 class="templow_text"><?php print $forecastinfo[0]->tempMinC; ?></h3>
              </li>
            </ul>
          </div>
        <?php } ?>
        </div>
      </div>
    </div>
    <div id="changelocationdiv">
      <label>
        <select id="changelocation">
        <?php
        if (count($locations) > 0) {
          foreach ($locations as $key => $value) {
            if ($selected_location == $value) {
            ?>
              <option value="<?php print $key; ?>" selected="selected"><?php print $value; ?></option>
            <?php } else { ?>
              <option value="<?php print $key; ?>"><?php print $value; ?></option>
            <?php
            }
          }
        }
        ?>
        </select>
      </label>
    </div>
  </div>
</div>
<input type="hidden" id="current_display_icon" value="<?php print $forecastinfo[0]->weatherCode; ?>" />
