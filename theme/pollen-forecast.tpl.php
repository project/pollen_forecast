<div class="pollen-set">
  <div class="pollen-weather">
    <?php if ($display_pollen_count): ?>
    <p><?php print t('Date')?> : <?php print $forecastdata->date; ?></p>
    <p><?php print t('Pollen')?> : <?php print $forecastdata->pollenoutlook; ?></p>
    <?php endif; ?>
    <p><?php print t('Weather')?> : <?php print $forecastdata->weatherDesc; ?> <?php print $forecastdata->weatherIconUrl; ?></p>
  </div>
  <div class="pollen-additional-details">
    <p><?php print t('Temperature')?> : <?php print $forecastdata->tempMinC; ?> &mdash; <?php print $forecastdata->tempMaxC; ?></p>
    <p><?php print t('Wind Speed / Direction')?> : <?php print $forecastdata->windspeed; ?> / <?php print $forecastdata->winddir; ?></p>
    <p><?php print t('Humidity / Cloudcover')?> : <?php print $forecastdata->humidity; ?>/ <?php print $forecastdata->cloudcover; ?></p>
    <p><?php print t('Pressure')?> : <?php print $forecastdata->pressure; ?></p>
  </div>
</div>
