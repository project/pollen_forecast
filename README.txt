This module will allow us to figure out Pollen details of the cities along
with the weather forecast.Pollen count is a measure of how much pollen is in
the air in a certain area at a specific time. It is expressed in grains of
pollen per square meter of air collected over 24 hours.

===================
    INSTALLATON
===================


===================
       SETUP
===================
For configuring your module to fetch pollen forecast, go to the
admin/config/services/pollen_forecast page on your admin site, fill in the
details related the application and submit it.


===================
   Known Issues
===================
Pollen forecast is not supported on older browsers. Your users must use
Internet Explorer 10+ (or Edge), Chrome 30+, Firefox 28+, or Safari 7+.
