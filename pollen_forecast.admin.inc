<?php

/**
 * @file
 * Administration form setup for Pollen forecast.
 */

/**
 * Builds the Admin form.
 */
function pollen_forecast_admin_form($form, &$form_state) {

  //set general warning messages
  drupal_set_message(t('<b>Warning!</b> Changing values in the form will affect pollen forecast. So please do not change the values with out administrator assistance.'), 'warning');

  $form['pollen_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pollen Forecast Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );

  $form['pollen_settings']['available_regions'] = array(
    '#type' => 'textarea',
    '#title' => t('Available Regions'),
    '#description' => t('Enter one value per line, in the format key|label.
      The key is the value that will be stored in the database, and it must
      match the field storage type (text). The label is optional, and the key
      will be used as the label if no label is specified.'),
    '#default_value' => variable_get('available_regions', '')
  );

  $form['weather_settings'] = array(
    '#title' => t('Weather Icon Grouping'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  $form['weather_settings']['weather_icons'] = array(
    '#type' => 'textarea',
    '#title' => t('Weather Icon Grouping'),
    '#description' => t('Enter one value per line, in the format key|label.
      The key is the value of the Weather code that will be stored in the database,
      and it must match the field storage type (text). The label is optional, and the
      key will be used as the label if no label is specified.'),
    '#default_value' => variable_get('weather_icons', '')
  );

  $form['setpollendisplay'] = array(
    '#title' => t('Display Pollen Forecast'),
    '#type' => 'radios',
    '#required' => true,
    '#options' => array(
        1 => t('Yes'),
        0 => t('No'),
     ),
    '#default_value' => variable_get('setpollendisplay', 0)
  );


  return system_settings_form($form);
}


/**
 * Function to implement the allergy forecast configuration form
 */
function pollenforecast_admin_form() {
  $form = array();

  $form['f_geonames'] = array(
    '#title' => t('Configure Proxy Settings for Geonames API'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );

  $form['f_geonames']['pollen_geoipsearch'] = array(
    '#title' => t('Enable Geo IP Search'),
    '#type' => 'checkbox',
    '#description' => t('Checking this will enable the Geo IP search for the
      users to get the location dynamically and load the pollen forecast.
      Else user will view the below selected location by default.'),
    '#default_value' => variable_get('pollen_geoipsearch', 0)
  );

  $regions = get_forecast_regions();

  $form['f_geonames']['pollen_defaultlocation'] = array(
    '#title' => t('Default Location'),
    '#type' => 'select',
    '#options' => $regions,
    '#description' => t('Select the default location that needs to be displayed
      if the user location is not available in the list'),
    '#default_value' => variable_get('pollen_defaultlocation', '')
  );
  $form['f_pollenproxy'] = array(
    '#title' => t('Configure Proxy Settings for Pollen Outlook API'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );

  $form['f_pollenproxy']['pollen_cachingdates'] = array(
    '#title' => t('Caching Dates'),
    '#type' => 'checkboxes',
    '#options' => array(
      'Mon' => t('Mon'),
      'Tue' => t('Tue'),
      'Wed' => t('Wed'),
      'Thu' => t('Thu'),
      'Fri' => t('Fri'),
      'Sat' => t('Sat'),
      'Sun' => t('Sun')
    ),
    '#default_value' => variable_get('pollen_cachingdates', array())
  );
  $form['f_forcastproxy'] = array(
    '#title' => t('Configure Proxy Settings for WorldWeatherOnline API'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  $form['f_forcastproxy']['pollen_forcastkey'] = array(
    '#title' => t('API Key'),
    '#type' => 'textfield',
    '#default_value' => variable_get('pollen_forcastkey', '')
  );
  $form['f_forcastproxy']['pollen_forcastfeedkey'] = array(
    '#title' => t('API Feed Key'),
    '#type' => 'textfield',
    '#default_value' => variable_get('pollen_forcastfeedkey', '')
  );
  $form['f_forcastproxy']['pollen_forcastdays'] = array(
    '#title' => t('No. of Days'),
    '#type' => 'textfield',
    '#default_value' => variable_get('pollen_forcastdays', '')
  );
  $form['f_forcastproxy']['pollen_geourl'] = array(
    '#title' => t('Geo Location API URL'),
    '#type' => 'textfield',
    '#default_value' => variable_get('pollen_geourl', '')
  );
  $form['f_forcastproxy']['pollen_pollenurl'] = array(
    '#title' => t('Pollen Outlook URL'),
    '#type' => 'textfield',
    '#default_value' => variable_get('pollen_pollenurl', '')
  );
  $form['f_forcastproxy']['pollen_forcasturl'] = array(
    '#title' => t('Weather Forecast URL'),
    '#type' => 'textfield',
    '#default_value' => variable_get('pollen_forcasturl', '')
  );

  return system_settings_form($form);
}

/**
 * Function to implement pollen_forecast_logs
 */
function pollen_forecast_logs() {
  $form = array();

  $form['log_geonamesapi'] = array(
    '#title' => t('Log Geonames API Response'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('pollen_log_geonamesapi', 0)
  );
  $form['log_pollenapi'] = array(
    '#title' => t('Log Pollen Outlook API Response'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('pollen_log_pollenapi', 0)
  );
  $form['log_weatherapi'] = array(
    '#title' => t('Log Weather API Response'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('pollen_log_weatherapi', 0)
  );

  return system_settings_form($form);
}
